package main

import (
	"encoding/json"
	"fmt"
)

func Unmarshal[T any](data []byte, val *T) error {
	return json.Unmarshal(data, val)
}

func main() {
	data := []byte(`[1, 2, 3]`)
	var v []int
	fmt.Println(Unmarshal(data, &v))
	// fmt.Println(Unmarshal(data, v)) // won't compile
	fmt.Println(v)
}

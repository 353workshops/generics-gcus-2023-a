# Practical Generics


Miki Tebeka
📬 [miki@353solutions.com](mailto:miki@353solutions.com), 𝕏 [@tebeka](https://twitter.com/tebeka), 👨 [mikitebeka](https://www.linkedin.com/in/mikitebeka/), ✒️[blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [Go Essential Training](https://www.linkedin.com/learning/go-essential-training/) - LinkedIn Learning
    - [Rest of classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Go Brain Teasers](https://pragprog.com/titles/d-gobrain/go-brain-teasers/) book

---

[Final Exercise](_extra/must.md)

### Code

- [handler.go](handler/handler.go) - Limitation of generics (common field)
- [decode.go](decode/decode.go) - Compile time check for pointer
- [filter.go](filter/filter.go) - How generics makes user code clearer
- [ptr.go](ptr/ptr.go) - Inline pointers
- [middleware](middleware/middleware.go) - Load JSON & Validate HTTP middleware
- [scanner.go](scanner/scanner.go) - Unmarshalling different types
- [cache.go](cache/cache.go) - Generic cache
- [stack.go](stack/stack.go) - Generic data structure
- [encode.go](encode/encode.go) - Show generated functions
    - `dlv debug encode.go`
    - `funcs main.Encode`
- [escape.go](escape/escape.go) - Interfaces escape to the heap
- [relu.go](deep/relu.go) - Generic functions


### Links

- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)
- [An Introduction to Generics](https://go.dev/blog/intro-generics)
- [Generics the Hard Way](https://github.com/akutz/go-generics-the-hard-way/)
- [All your comparable types](https://go.dev/blog/comparable)
    - [comparable](https://pkg.go.dev/builtin#comparable) built-in type
- [Generics can make your Go code slower](https://planetscale.com/blog/generics-can-make-your-go-code-slower)
- [Type constriants](https://go.dev/ref/spec#Type_constraints) in the Go language specification
- Packages
    - [constraints](https://pkg.go.dev/constraints)
    - [maps](https://pkg.go.dev/maps)
    - [slices](https://pkg.go.dev/slices)
    - [hashicorp/golang-lru](https://pkg.go.dev/github.com/hashicorp/golang-lru/v2)
    - [lo](https://github.com/samber/lo)
    - [flags](https://github.com/tebeka/flags)
- [Type Parameters](https://go.dev/doc/faq#Type_Parameters) in the Go FAQ
- [proposal: iter: new package for iterators](https://github.com/golang/go/issues/61897)

package deep

import "fmt"

func Relu(n any) any {
	switch v := n.(type) {
	case int:
		if v < 0 {
			return 0
		}
		return v
	case float64:
		if v < 0 {
			return 0
		}
		return v
	}

	panic(fmt.Sprintf("Relu on unsupported type: %#v of %T", n, n))
}

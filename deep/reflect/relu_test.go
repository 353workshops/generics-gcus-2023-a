package deep

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

var reluCases = []struct {
	n      any
	out    any
	panics bool
}{
	{1, 1, false},
	{-1, 0, false},
	{1.1, 1.1, false},
	{uint(3), 0, true},
}

func TestRelu(t *testing.T) {
	for _, tc := range reluCases {
		name := fmt.Sprintf("%#v (panics=%v)", tc.n, tc.panics)
		t.Run(name, func(t *testing.T) {
			if tc.panics {
				require.Panics(t, func() { Relu(tc.n) })
				return
			}

			out := Relu(tc.n)
			require.Equal(t, tc.out, out)
		})
	}
}

package deep

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

var maxCases = []struct {
	val []int
	err bool
}{
	{[]int{3, 2, 1}, false},
}

/*
func genMaxCases[T Number]() []testCase[T] {
	...
}
*/

func TestMax(t *testing.T) {
	_, err := Max[int](nil)
	require.Error(t, err)

	intOut, err := Max([]int{3, 2, 1})
	require.NoError(t, err)
	require.Equal(t, 3, intOut)

	floatOut, err := Max([]float64{3, 2, 1})
	require.NoError(t, err)
	require.Equal(t, 3.0, floatOut)

	monOut, err := Max([]time.Month{3, 2, 1})
	require.NoError(t, err)
	require.Equal(t, 3, monOut)

	strOut, err := Max([]string{"C", "A", "B"})
	require.NoError(t, err)
	require.Equal(t, "C", strOut)
}

func TestReluInt(t *testing.T) {
	out := Relu(7)
	require.Equal(t, 7, out)
	out = Relu(-2)
	require.Equal(t, 0, out)
	fout := Relu(-2.0)
	require.Equal(t, 0.0, fout)
}

/*
func TestReluInt(t *testing.T) {
	out := ReluInt(7)
	require.Equal(t, 7, out)
	out = ReluInt(-2)
	require.Equal(t, 0, out)
}

func TestReluFloat64(t *testing.T) {
	out := ReluInt(7)
	require.Equal(t, 7, out)
	out = ReluInt(-2)
	require.Equal(t, 0, out)
}

*/

package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"strings"
)

// int -> Int
func titleize(name string) string {
	return strings.ToUpper(name[:1]) + name[1:]
}

var funcTemplate = `
func Relu%s(n %s) %s {
	if n < 0 {
		return 0
	}
	return n
}
`

func main() {
	var outFile string
	flag.StringVar(&outFile, "out", "", "output file")
	flag.Usage = func() {
		name := path.Base(os.Args[0])
		fmt.Fprintf(os.Stderr, "Usage: %s [OPTION]... TYPE [TYPE]...\n", name)
		fmt.Println("Options:")
		flag.PrintDefaults()
	}
	flag.Parse()
	if flag.NArg() == 0 {
		fmt.Fprintln(os.Stderr, "error: missing type(s)")
		os.Exit(1)
	}

	out := os.Stdout
	if outFile != "" {
		var err error
		out, err = os.Create(outFile)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %s\n", err)
			os.Exit(1)
		}
	}

	fmt.Fprintln(out, "package deep")
	for _, typ := range flag.Args() {
		name := titleize(typ)
		fmt.Fprintf(out, funcTemplate, name, typ, typ)
	}
}

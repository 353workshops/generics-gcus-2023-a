package deep

func ReluInt(n int) int {
	if n < 0 {
		return 0
	}
	return n
}

func ReluFloat64(n float64) float64 {
	if n < 0 {
		return 0
	}
	return n
}

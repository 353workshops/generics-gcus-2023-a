package deep

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestReluInt(t *testing.T) {
	out := ReluInt(7)
	require.Equal(t, 7, out)
	out = ReluInt(-2)
	require.Equal(t, 0, out)
}

func TestReluFloat64(t *testing.T) {
	out := ReluInt(7)
	require.Equal(t, 7, out)
	out = ReluInt(-2)
	require.Equal(t, 0, out)
}

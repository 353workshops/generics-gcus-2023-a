package deep

import (
	"errors"
)

var ErrEmpty = errors.New("empty slice")

// exercise
func Mode[T comparable](values []T) (T, error) {
	// var c comparable // won't compile
	if len(values) == 0 {
		return zero[T](), ErrEmpty
	}
	counts := make(map[T]int)
	for _, v := range values {
		counts[v]++
	}

	maxN, maxV := 0, zero[T]()
	for v, n := range counts {
		if n > maxN {
			maxN, maxV = n, v
		}
	}

	return maxV, nil
}

/*
type Month int // Month is differnt type
type Month = int // Month is a differnt name to int
*/

type Ordered interface {
	//	Encode() ([]byte, error)
	~int | ~float64 | ~string
}

func zero[T any]() T {
	var z T
	return z
}

// exercise (support float64)
func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		// var zero T
		return zero[T](), ErrEmpty
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}

	return m, nil
}

func Relu[T Ordered](n T) T {
	if z := zero[T](); n < z {
		return z
	}

	return n
}

// GCShape stenciling
// Two concrete types are the in the same gcshape group
// iff they have the same underlying type
// All pointers go to the same gcshape group
type Month int

/*
func ReluInt(n int) int {
	if n < 0 {
		return 0
	}
	return n
}

func ReluFloat64(n float64) float64 {
	if n < 0 {
		return 0
	}
	return n
}

*/

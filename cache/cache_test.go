package cache

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCacheBadSize(t *testing.T) {
	_, err := NewCache[string, int](-1)
	require.Error(t, err)

	_, err = NewCache[string, int](0)
	require.Error(t, err)
}

func keyOf(i int) string {
	return fmt.Sprintf("key-%02d", i)
}

func TestCache(t *testing.T) {
	const size = 3
	c, err := NewCache[string, int](size)
	require.NoError(t, err)
	require.Equal(t, 0, c.Len())

	const n = size + 2
	for i := 0; i < n; i++ {
		c.Set(keyOf(i), i)
	}
	require.Equal(t, size, c.Len())

	// dropped keys
	for i := 0; i < n-size; i++ {
		_, ok := c.Get(keyOf(i))
		require.False(t, ok)
	}

	// existing keys
	for i := n - size; i < size; i++ {
		v, ok := c.Get(keyOf(i))
		require.Equal(t, i, v)
		require.True(t, ok)
	}
}

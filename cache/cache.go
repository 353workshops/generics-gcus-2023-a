package cache

import (
	"fmt"
	"time"
)

type entry[T any] struct {
	val  T
	time time.Time
}

type Cache[K comparable, V any] struct {
	m    map[K]entry[V]
	size int
}

func NewCache[K comparable, V any](size int) (*Cache[K, V], error) {
	if size <= 0 {
		return nil, fmt.Errorf("size must be > 0 (got %d)", size)
	}
	c := Cache[K, V]{
		m:    make(map[K]entry[V]),
		size: size,
	}
	return &c, nil
}

func (c Cache[K, V]) dropOne() {
	minT := time.Now()
	var minK K
	for k, e := range c.m {
		if e.time.Before(minT) {
			minK, minT = k, e.time
		}
	}

	delete(c.m, minK)
}

func (c Cache[K, V]) Set(key K, v V) {
	if len(c.m) == c.size {
		c.dropOne()
	}
	c.m[key] = entry[V]{v, time.Now()}
}

func (c Cache[K, V]) Get(key K) (V, bool) {
	e, ok := c.m[key]
	if !ok {
		var zero V
		return zero, false
	}

	return e.val, true
}

func (c Cache[K, V]) Len() int {
	return len(c.m)
}

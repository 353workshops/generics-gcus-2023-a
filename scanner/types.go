package scanner

import (
	"fmt"
	"time"
)

type Login struct {
	Time  time.Time `json:"time"`
	Login string    `json:"login"`
}

func (l Login) Validate() error {
	if l.Time.Equal(time.Time{}) {
		return fmt.Errorf("missing Time")
	}
	if l.Login == "" {
		return fmt.Errorf("missing Login")
	}
	return nil
}

type Access struct {
	Time   time.Time `json:"time"`
	Login  string    `json:"login"`
	URI    string    `json:"uri"`
	Action string    `json:"action"`
}

func (a Access) Validate() error {
	if a.Time.Equal(time.Time{}) {
		return fmt.Errorf("missing Time")
	}
	if a.Login == "" {
		return fmt.Errorf("missing Login")
	}
	if a.URI == "" {
		return fmt.Errorf("missing URI")
	}
	if a.Action == "" {
		return fmt.Errorf("missing Action")
	}

	return nil
}

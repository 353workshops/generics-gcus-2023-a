package scanner

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestScannerLogin(t *testing.T) {
	// GoLand: "scanner/testdadta/logins.jsonl" ?
	file, err := os.Open("testdata/logins.jsonl")
	require.NoError(t, err, "open")
	defer file.Close()

	s := New[Login](file)
	var logs []Login
	var l Login
	for s.Next(&l) {
		logs = append(logs, l)
	}
	t.Logf("users: %#v\n", logs)
	require.NoError(t, s.Err(), "scan")
	require.Equal(t, 3, len(logs))
	for _, u := range logs {
		require.NoError(t, u.Validate())
	}
}

func TestScannerAccess(t *testing.T) {
	file, err := os.Open("testdata/access.jsonl")
	require.NoError(t, err, "open")
	defer file.Close()

	s := New[Access](file)
	var logs []Access
	var l Access
	for s.Next(&l) {
		logs = append(logs, l)
	}
	t.Logf("users: %#v\n", logs)
	require.NoError(t, s.Err(), "scan")
	require.Equal(t, 3, len(logs))
	for _, u := range logs {
		require.NoError(t, u.Validate())
	}
}

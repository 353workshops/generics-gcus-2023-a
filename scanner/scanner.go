package scanner

import (
	"encoding/json"
	"errors"
	"io"
)

type Event interface {
	Login | Access
}

// Exercise: support also Access in Scanner
// type Scanner struct {
type Scanner[T any] struct {
	dec *json.Decoder
	err error
}

func New[T any](r io.Reader) *Scanner[T] {
	s := Scanner[T]{
		dec: json.NewDecoder(r),
	}
	return &s
}

func (s *Scanner[T]) Err() error {
	return s.err
}

type Validator interface {
	Validate() error
}

// func (s *Scanner) Next() (l *Login, bool) {
// func (s *Scanner) Next(l Validator) bool {
func (s *Scanner[T]) Next(l *T) bool {
	if s.err != nil {
		return false
	}

	err := s.dec.Decode(l)
	if errors.Is(err, io.EOF) {
		return false
	}

	if err != nil {
		s.err = err
		return false
	}

	/* When accepting Validator
	if err := l.Validate(); err != nil {
		s.err = err
		return false
	}
	*/
	if v, ok := any(l).(Validator); ok {
		if err := v.Validate(); err != nil {
			s.err = err
			return false
		}
	}

	return true
}

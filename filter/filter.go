package main

import (
	"fmt"
)

type Validator interface {
	Validate() error
}

type Port int

const (
	minPort = 0
	maxPort = 65_535
)

func (p Port) Validate() error {
	if p < minPort || p > maxPort {
		return fmt.Errorf("port %d out of range [%d,%d]", p, minPort, maxPort)
	}
	return nil
}

// func Filter(vs []Validator) []Validator {
func Filter[T Validator](vs []T) []T {
	var valid []T

	for _, v := range vs {
		if err := v.Validate(); err != nil {
			continue
		}
		valid = append(valid, v)
	}
	return valid
}

func main() {
	ports := []Port{80, 8080, 99_999}
	// ports := []Validator{Port(80), Port(8080), Port(99_999)}
	validPorts := Filter(ports)
	fmt.Println("valid:", validPorts)
	if validPorts[0] < 1024 {
		// p := ports[0].(Port)
		// if p < 1024 {
		fmt.Println("privileged port")
	}
}

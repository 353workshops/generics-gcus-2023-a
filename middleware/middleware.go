package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

type ctxKeyType struct{}

var ctxKey ctxKeyType

type Validator interface {
	Validate() error
}

func dataMiddleware[T Validator](h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var val T
		if err := json.NewDecoder(r.Body).Decode(&val); err != nil {
			log.Printf("ERROR: bad JSON - %s", err)
			http.Error(w, "can't decode", http.StatusBadRequest)
			return
		}

		if err := val.Validate(); err != nil {
			log.Printf("ERROR: bad log data in %#v", val)
			http.Error(w, "bad data", http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), ctxKey, val)
		r = r.WithContext(ctx)
		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

type Log struct {
	Level   string `json:"level"`
	Message string `json:"message"`
}

func (l *Log) Validate() error {
	if l.Level == "" {
		return fmt.Errorf("missing level")
	}
	if l.Message == "" {
		return fmt.Errorf("missing message")
	}
	return nil
}

func logHandler(w http.ResponseWriter, r *http.Request) {
	l, ok := r.Context().Value(ctxKey).(*Log)
	if !ok {
		http.Error(w, "missing user", http.StatusBadRequest)
		return
	}

	fmt.Fprintf(w, "%+v\n", l)
}

func main() {
	h := dataMiddleware[*Log](http.HandlerFunc(logHandler))
	http.Handle("/log", h)

	addr := ":" + os.Getenv("ADDR")
	if addr == ":" {
		addr = ":8080"
	}
	log.Printf("INFO: server starting on %s", addr)
	if err := http.ListenAndServe(addr, nil); err != nil {
		log.Fatalf("error: can't start server - %s", err)
	}
}

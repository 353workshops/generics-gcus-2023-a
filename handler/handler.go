package main

import "log"

type Access struct {
	User string
	URI  string
}

type Logout struct {
	User string
}

type Event interface {
	Access | Logout

	User() string
}

func Handle[T Access | Logout](e T) {
	user := ""
	switch v := any(e).(type) {
	case Access:
		user = v.User
	case Logout:
		user = v.User
	}
	// log.Printf("handling event %T from %s", e, e.User)
	log.Printf("handling event %T from %s", e, user)
}

func main() {
	Handle(Logout{"elliot"})
}

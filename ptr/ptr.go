package main

import "fmt"

type Player struct {
	Name    string  `json:"name,omitempty"`
	TagLine *string `json:"tag_line,omitempty"`
}

func Ptr[T any](v T) *T {
	return &v
}

func main() {
	// tl := "Suit up!"
	p := Player{
		Name:    "Barney",
		TagLine: Ptr("Suit up!"),
		// TagLine: &tl,
		// TagLine: &"Suit up!",
		// TagLine: &tl,
	}
	fmt.Println(p)
}
